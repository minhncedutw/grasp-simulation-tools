## What is this?

This is guide to install a simulation toolbox for training and testing robot grasping.
It is the simplest installation one I've ever found out.

---

## How to install it?

1. [Download and Install](https://www.docker.com/community-edition#/download/) **Docker**.
2. After install **Docker**, to download the simulation toolbox to local, open a terminal then type: docker pull shadowrobot/smart_grasping_sandbox
3. The toolbox is automatically downloaded and it is already installed in the docker container. You just use for simulating.

---

## How to begin using it?

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. To use it, firstly, you have to run the docker container that contains the toolbox by typing in the terminal: docker run -it --name sgs -p 8080:8080 -p 8888:8888 -p 8181:8181 -p 7681:7681 shadowrobot/smart_grasping_sandbox
2. To see the simulation, open a web browser and type: [localhost:8080](http://localhost:8080/)
3. To open an iPython Notebook to code controlling simulated robot, on a web browser type: [localhost:8888](http://localhost:8888/) . It will require a password, type: shadow . 
4. There are some starting samples inside, just run it to understand

I hope you can be familiar with it soon.
Thank you

Resource:
https://medium.freecodecamp.org/an-open-sandbox-for-robot-grasping-cee467a3fabb/
https://hub.docker.com/r/shadowrobot/smart_grasping_sandbox/
https://github.com/shadow-robot/smart_grasping_sandbox/